function ind = detectEvents(trigSignal,thresh)
%TRIGEVENTS Return indices of events found in trigSignal
%
% Looks into signal for local maxima (inflection points)
% Data must :
% be "smoothed", either with a filter or with a x-correlation
%
% trig signal can be a (NxM) matrix with N "time" index and M repetition
%
% Fabrice merenda
% Last revision 30.05.2013

%% Default

if size(trigSignal,1)<size(trigSignal,2)
    trigSignal = trigSignal';
end

%% Event detection

% Compute fisrt and second derivative of the signal
diffTrigSignal = diff(trigSignal); %(N-1)*M
diff2TrigSignal = diff(trigSignal,2); %(N-2)*M

% isAbove: point is above threshold
isAbove = logical(trigSignal(2:end-1)>thresh); %(N-2)xM

% isExtrema: look for 1st derivative zero crossings
isExtrema = logical((diffTrigSignal(1:end-1,:).*diffTrigSignal(2:end,:))<0); %(N-2)xM

% isMaximum: look for 2nd derivative sign
isMaximum = logical(diff2TrigSignal<0);%(N-2)xM

%% Return indices

isAll = isAbove & isExtrema & isMaximum; %(N-2)xM
ind = find(isAll==true)+1;

%% Plot if no output

if nargout==0
    plot(trigSignal)
    line([ind ind],ylim)
end

end

